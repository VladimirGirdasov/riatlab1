﻿namespace RiatLab1
{
    using System;

    [Obsolete("Lab 2 entity. No need")]
    public enum RequestType
    {
        GET,
        POST,
        PUT,
        PATCH
    }
}
