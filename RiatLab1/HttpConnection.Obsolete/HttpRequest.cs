﻿namespace RiatLab1
{
    using System;
    using System.IO;
    using System.Net;
    using System.Text;

    [Obsolete("Lab 2 entity. No need")]
    public class HttpRequest
    {
        private HttpWebRequest request;

        public string ResponseBody { get; set; }
        private readonly string body;
        private readonly string uri;
        private readonly int timeoutMs;
        private readonly RequestType requestType;

        public HttpRequest(RequestType requestType, string url, string path, int timeoutMs = 1000)
        {
            uri = url + path;
            this.timeoutMs = timeoutMs;
            this.requestType = requestType;
            body = string.Empty;
        }

        public HttpRequest(RequestType requestType, string url, string path, string body, int timeoutMs = 1000)
        {
            uri = url + path;
            this.timeoutMs = timeoutMs;
            this.requestType = requestType;
            this.body = body;
        }

        private void RebuildRequest()
        {
            request = (HttpWebRequest)WebRequest.Create(uri);
            request.Timeout = timeoutMs;
            request.Method = requestType.ToString();

            if (!string.IsNullOrEmpty(body))
                SetBody();
        }

        private void SetBody()
        {
            var data = Encoding.UTF8.GetBytes(body);
            request.ContentLength = data.Length;

            using (var stream = request.GetRequestStream())
            {
                stream.Write(data, 0, data.Length);
            }
        }

        public string Execute(bool force)
        {
            RebuildRequest();

            if (force)
            {
                while (!TryGetResponse())
                {
                    RebuildRequest();
                }
            }
            else
                TryGetResponse();

            return ResponseBody;
        }

        private bool TryGetResponse()
        {
            try
            {
                var response = (HttpWebResponse)request.GetResponse();

                using (var streamReader = new StreamReader(response.GetResponseStream(), Encoding.UTF8))
                {
                    ResponseBody = streamReader.ReadToEnd();
                    return true;
                }
            }
            catch (WebException e)
            {
                if (e.Status != WebExceptionStatus.Timeout &&
                    e.Status != WebExceptionStatus.ReceiveFailure &&
                    e.Status != WebExceptionStatus.NameResolutionFailure)
                    throw;
                ResponseBody = null;
                return false;
            }
        }
    }
}