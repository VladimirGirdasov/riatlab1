﻿namespace RiatLab1
{
    using System;

    [Obsolete("Lab 2 entity. No need")]
    public class HttpConnection
    {
        private readonly string url;

        public HttpConnection(string domainName, string port)
        {
            url = $"http://{domainName.Trim('/')}";
            if (!string.IsNullOrEmpty(port))
                url += $":{port}";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="force">Повторять выполнение запроса, пока не перестанем получать в ответ ошибки</param>
        /// <param name="requestType">Тип запроса</param>
        /// <param name="urlPath">url path</param>
        /// <param name="requestBody">Тело запроса</param>
        /// <param name="timeoutMs">request timeout</param>
        /// <returns>Полученное в ответ тело</returns>
        public string SendRequest(bool force, RequestType requestType, string urlPath, string requestBody, int timeoutMs = 1000)
        {
            var request = new HttpRequest(requestType, url, urlPath, requestBody);
            return request.Execute(force);
        }

        public string SendRequest(bool force, RequestType requestType, string urlPath, int timeoutMs = 1000)
        {
            var request = new HttpRequest(requestType, url, urlPath);
            return request.Execute(force);
        }
    }
}
