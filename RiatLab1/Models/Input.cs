﻿namespace RiatLab1
{
    using System;
    using System.Linq;

    public class Input
    {
        public int K { get; set; }
        public decimal[] Sums { get; set; }
        public int[] Muls { get; set; }
        
        public Output TransformToOutput()
        {
            return new Output()
            {
                SumResult = Sums.Sum() * K,
                MulResult = Muls.Aggregate(1, (x, y) => x * y),
                SortedInputs = Sums.Concat(Array.ConvertAll(Muls, x => (decimal)x)).OrderBy(x => x).ToArray()
            };
        }
    }
}
