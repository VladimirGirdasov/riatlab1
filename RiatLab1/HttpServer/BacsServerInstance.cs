﻿using RiatLab1.HttpServer.Interfaces;

namespace RiatLab1.HttpServer
{
    using System.Collections.Generic;
    using System.IO;
    using System.Net;
    using System.Text;

    public class BacsServerInstance : IHttpServerInstance
    {
        private Serializer.Json serializer;

        public Serializer.Json Serializer => serializer ?? (serializer = new Serializer.Json());

        private Input input;

        public void Ping(HttpListener httpListener, HttpListenerContext context)
        {
            SetContextStatus(context, HttpStatusCode.OK);
        }

        public void PostInputData(HttpListener httpListener, HttpListenerContext context)
        {
            using (var stream = new StreamReader(context.Request.InputStream))
            {
                input = Serializer.Deserialize<Input>(stream.ReadToEnd());
            }

            SetContextStatus(context, HttpStatusCode.OK);
        }

        public void GetAnswer(HttpListener httpListener, HttpListenerContext context)
        {
            var output = Serializer.Serialize(input.TransformToOutput());
            var outputBytes = Encoding.UTF8.GetBytes(output);

            using (var stream = context.Response.OutputStream)
            {
                stream.Write(outputBytes, 0, outputBytes.Length);
            }
        }

        public void Stop(HttpListener httpListener, HttpListenerContext context)
        {
            SetContextStatus(context, HttpStatusCode.OK);
            httpListener.Stop();
        }

        private static void SetContextStatus(HttpListenerContext context, HttpStatusCode status)
        {
            context.Response.StatusCode = (int) status;
            context.Response.Close();
        }
    }
}