﻿using System.Collections.Generic;

namespace RiatLab1.HttpServer
{
    using System;
    using System.Linq;
    using System.Net;
    using Interfaces;

    public class HttpServer : IHttpServer
    {
        public readonly string DomainName;

        public delegate void ServerAction(HttpListener httpListener, HttpListenerContext context);

        Dictionary<string, ServerAction> RouteConfig { get; }

        public HttpServer(IHttpServerInstance serverInstance, string hostName, string port)
        {
            DomainName = $"http://{hostName.Trim('/')}";
            if (!string.IsNullOrEmpty(port))
            {
                DomainName += $":{port}";
            }
            DomainName += "/";

            // todo: сформируйте этот список с помозщью рефлексии tyepof(T).GetMethods, тогда при добавлении нового метода, не нужно будет менять этот словарь. все заработает автоматом. Open/Closed принцип
            RouteConfig = new Dictionary<string, ServerAction>();

            foreach (var methodInfo in serverInstance.GetType().GetMethods())
            {
                RouteConfig.Add(methodInfo.Name, (listener, context) => methodInfo.Invoke(serverInstance, new object[] { listener, context }));
            }
        }

        public void Listen()
        {
            var httpListener = new HttpListener();
            httpListener.Prefixes.Add(DomainName);
            httpListener.Start();

            while (httpListener.IsListening)
            {
                try
                {
                    var context = httpListener.GetContext();
                    var methodToCall = CleanupActionName(context.Request.RawUrl);

                    if (RouteConfig.ContainsKey(methodToCall))
                    {
                        RouteConfig[methodToCall](httpListener, context);
                    }
                    else
                    {
                        // wrong request
                        context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    }
                }
                catch (Exception)
                {
                    // ignored
                }
            }
        }

        private static string CleanupActionName(string url)
        {
            // remove parameters
            url = url.Split('?').First();
            // remove anchors
            url = url.Split('#').First();

            return url.Trim('/');
        }
    }
}