﻿namespace RiatLab1.HttpServer.Interfaces
{
    public interface IHttpServer
    {
        void Listen();
    }
}