﻿namespace RiatLab1.HttpServer.Interfaces
{
    using System.Net;

    public interface IHttpServerInstance
    {
        //todo: Экшны в сигнатуре методов интерфейса очень плохач идея, тт.к. они протьиворечат ООП. Сделайте интерфейс с методами, которые обощначены в требованиях задачи, а внутри можете реализовать их с помощью экшнов
        //todo: делегаты прицнипиально от экшнов ничем не отличаются
        void Ping(HttpListener httpListener, HttpListenerContext context);

        void PostInputData(HttpListener httpListener, HttpListenerContext context);

        void GetAnswer(HttpListener httpListener, HttpListenerContext context);

        void Stop(HttpListener httpListener, HttpListenerContext context);
    }
}