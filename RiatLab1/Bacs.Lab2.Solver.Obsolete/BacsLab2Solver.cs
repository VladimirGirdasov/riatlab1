﻿namespace RiatLab1
{
    using System;

    [Obsolete("Lab 2 entity. No need")]
    public class BacsLab2Solver : HttpConnection, IBacsLab2Solver
    {
        private readonly Serializer.Json serializer;

        public BacsLab2Solver(string domainName, string port) : base(domainName, port)
        {
            serializer = new Serializer.Json();
        }

        public void Ping(bool force)
        {
            SendRequest(force, RequestType.GET, "/Ping");
        }

        public Input GetInputData(bool force)
        {
            var buf = SendRequest(force, RequestType.GET, "/GetInputData");
            return serializer.Deserialize<Input>(buf);
        }

        public void WriteAnswer(bool force, Output output)
        {
            SendRequest(force, RequestType.POST, "/WriteAnswer", serializer.Serialize(output));
        }
    }
}
