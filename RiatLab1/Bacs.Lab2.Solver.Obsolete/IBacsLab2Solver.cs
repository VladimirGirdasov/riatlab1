﻿namespace RiatLab1
{
    using System;

    [Obsolete("Lab 2 entity. No need")]
    public interface IBacsLab2Solver
    {
        void Ping(bool force);
        Input GetInputData(bool force);
        void WriteAnswer(bool force, Output output);
    }
}
