﻿using System.Linq;
using System.Reflection.Emit;
using RiatLab1.HttpServer;

namespace RiatLab1
{
    using System;

    internal class Program
    {
        private static void Main(string[] args)
        {
            var bacsInstance = new BacsServerInstance();
            var server = new HttpServer.HttpServer(bacsInstance, Resource.localhost, Console.ReadLine());
            server.Listen();
        }
    }
}
