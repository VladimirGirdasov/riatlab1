﻿namespace RiatLab1
{
    internal interface ISerialize
    {
        string Serialize<TObjectType>(TObjectType objectToSerialize);

        TObjectType Deserialize<TObjectType>(string data);
    }
}
