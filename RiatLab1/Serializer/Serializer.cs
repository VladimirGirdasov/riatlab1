﻿namespace RiatLab1
{
    using System;
    using System.IO;
    using System.Xml;
    using System.Xml.Serialization;
    using Newtonsoft.Json;

    public class Serializer
    {
        public class Xml : ISerialize
        {
            public string Serialize<TObjectType>(TObjectType objectToSerialize)
            {
                var serializer = new XmlSerializer(typeof(TObjectType));
                var emptyNamepsaces = new XmlSerializerNamespaces(new[] { XmlQualifiedName.Empty });
                var settings = new XmlWriterSettings
                {
                    Indent = true,
                    OmitXmlDeclaration = true
                };

                using (var stream = new StringWriter())
                {
                    using (var writer = XmlWriter.Create(stream, settings))
                    {
                        serializer.Serialize(writer, objectToSerialize, emptyNamepsaces);
                        return stream.ToString();
                    }
                }
            }

            public TObjectType Deserialize<TObjectType>(string data)
            {
                TObjectType ans;

                try
                {
                    using (var fs = new StringReader(data))
                    {
                        var serializer = new XmlSerializer(typeof(TObjectType));
                        ans = (TObjectType)serializer.Deserialize(fs);
                    }
                }
                catch (Exception)
                {
                    throw new ArgumentException("incorrect XML string");
                }

                return ans;

            }
        }

        public class Json : ISerialize
        {
            public TObjectType Deserialize<TObjectType>(string data)
            {
                TObjectType ans;

                try
                {
                    ans = JsonConvert.DeserializeObject<TObjectType>(data);
                }
                catch (Exception)
                {
                    throw new ArgumentException("incorrect JSON string");
                }

                return ans;
            }

            public string Serialize<TObjectType>(TObjectType objectToSerialize)
            {
                return JsonConvert.SerializeObject(objectToSerialize);
            }
        }
    }
}
